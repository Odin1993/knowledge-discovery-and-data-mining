import math

class tf_idf:
    def __init__(self):
        self.docs = dict()
        self.docs[1] = "fast car highway road car"
        self.docs[2] = "car car bike fast fast"
        self.docs[3] = "road road highway fast wheel"
        self.docs[4] = "bike wheel car wheel"

        self.N = len(self.docs) # Total Number of Documents
        print(self.N)
        self.vocabulary = []
    
    def buildVocabulary(self):
        print("Vocabulary\n#############")
        for x in self.docs:
            words = self.docs[x].split()
            for y in words:
                if y not in self.vocabulary:
                    self.vocabulary.append(y)
        
        for x in self.vocabulary:
            print(x)

    def calculateTF(self):
        print("\nTF\n######################")
        termFrequency = dict()

        for x in self.vocabulary:
            freq = []
            for y in self.docs:
                words = self.docs[y].split()
                numberOfWords = len(words)
                cnt = 0
                for w in words:
                    if w == x:
                        cnt = cnt + 1
                freq.append(cnt / numberOfWords)
            termFrequency[x] = freq

        for x in self.vocabulary:
            print(x + " => ", end = '')
            for y in termFrequency[x]:
                print(str(y) + " ", end = '')
            print("")

        return termFrequency



    def calculateIDF(self):
        print("\nIDF\n######################")
        docFrequency = dict()
        for x in self.vocabulary:
            cnt = 0
            for y in self.docs:
                if x in self.docs[y]:
                    cnt = cnt + 1

            docFrequency[x] = cnt

        docIDF = dict()
        for x in self.vocabulary:
            docIDF[x] = math.log10(self.N / docFrequency[x])
            print(x+ ": " + str(docIDF[x]))
        
        return docIDF

    def calculateTFIDF(self, tf, idf):
        print("\nTF-IDF\n######################")
        for x in self.vocabulary:
            for i in range(len(tf[x])):
                tf[x][i] = tf[x][i] * idf[x]
        
        for x in self.vocabulary:
            print("%10s => " % x, end="")
            for y in tf[x]:
                print("%10.5f " % y, end = '')
            print("")

                



def main():
    calc = tf_idf()
    calc.buildVocabulary()
    tf  = calc.calculateTF()
    idf = calc.calculateIDF()
    tfidf= calc.calculateTFIDF(tf, idf)



if __name__ == '__main__':
    main()

'''
Answer: 

Vocabulary
#############
fast
car
highway
road
bike
wheel

TF
######################
fast => 0.2 0.4 0.2 0.0
car => 0.4 0.4 0.0 0.25
highway => 0.2 0.0 0.2 0.0
road => 0.2 0.0 0.4 0.0
bike => 0.0 0.2 0.0 0.25
wheel => 0.0 0.0 0.2 0.5

IDF
######################
fast: 0.12493873660829992
car: 0.12493873660829992
highway: 0.3010299956639812
road: 0.3010299956639812
bike: 0.3010299956639812
wheel: 0.3010299956639812

TF-IDF
######################
      fast =>    0.02499    0.04998    0.02499    0.00000
       car =>    0.04998    0.04998    0.00000    0.03123
   highway =>    0.06021    0.00000    0.06021    0.00000
      road =>    0.06021    0.00000    0.12041    0.00000
      bike =>    0.00000    0.06021    0.00000    0.07526
     wheel =>    0.00000    0.00000    0.06021    0.15051
'''

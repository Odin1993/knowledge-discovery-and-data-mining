import numpy as np 
import pandas as pd

dataframe = pd.read_csv('toy-example.csv')

weight = dataframe['Weight'].values
height = dataframe['Height'].values

print('weight standard deviation-->'+ str(weight.std()))
print('height standard deviation-->'+ str(height.std()))